export const LOGOUT_SUCCESS = 'Success logout!';
export const AVATAR_UPLOADED_SUCCESS = 'Avatar has been uploaded successfully.';
export const PASSWORD_CHANGED = 'Password has been changed successfully';
export const USER_DELETED = 'User has been deleted successfully!';
