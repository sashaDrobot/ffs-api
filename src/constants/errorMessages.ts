export const PAGE_NOT_FOUND = 'Not found.';

export const TOKEN_NOT_PROVIDED = 'Authorization token is not provided.';
export const TOKEN_EXPIRED = 'Token has expired.';
export const TOKEN_INVALID = 'Invalid token.';

export const USER_NOT_FOUND = 'User not found.';
export const USER_EXISTS = 'User with this email already exists.';
export const USER_NOT_EXISTS = 'User with this email does not exists.';
export const PASSWORD_NOT_CORRECT = 'Password is not correct.';

export const SKILL_EXISTS = 'Skill already exists.';

export const IMAGE_NOT_VALID_TYPE = 'Only .png, .svg, .jpg and .jpeg format allowed!';
